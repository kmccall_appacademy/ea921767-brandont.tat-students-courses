class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end
  
  def name
    "#{first_name} #{last_name}"
  end

  def enroll(new_course)
    if courses.any?{|course| new_course.conflicts_with?(course)}
      raise_error "Course conflicts with already enrolled course"
    end

    courses << new_course unless courses.include?(new_course)
    new_course.students << self
  end

  def course_load
    department_units = Hash.new(0)

    courses.each do |course|
      department_units[course.department] += course.credits
    end

    department_units
  end
end
